import Menu from './components/Menu';
import Main from './components/Main';
import Home from './components/Home';
import {BrowserRouter as Router, Route, Switch,Redirect} from 'react-router-dom'
import Vedio from './components/Vedio';
import Account from './components/Account';
import Auth from './components/Auth';
import Posts from './components/Posts';
import NotFound from './components/NotFound'
import Welcome from './components/Welcome';
import React, { Component } from 'react'
export default class App extends Component {
    state = {
        isSubmited: false,
      };
    
    handleSubmit=()=>{
      this.setState({
        isSubmited:true
      })
    };
    render() {
        return (
            <div>
                <div>
            <Router>
                 <Menu/>
                <Switch>
                    <Route exact path="/" component={Main}/>
                    <Route path="/Home" component={Home}/>
                    <Route path="/Vedio" exact component={Vedio}/>
                    <Route path="/Vedio/:id" exact component={Vedio}/>
                    <Route path="/Vedio/:id/:category" exact component={Vedio}/>
                    <Route path="/Account"  component={Account}/>
                    <Route path="/Posts/:id" component={Posts}/>
                    <Route path="/Auth" render={()=><Auth handleSubmit={this.handleSubmit}/>} />
                    <Route path="/Welcome">
                        {this.state.isSubmited? <Welcome /> : <Redirect to="/Auth" />}
                    </Route>
                    <Route path="*" component={NotFound}/>
                </Switch>

            </Router>

        </div>
            </div>
        )
    }
}
