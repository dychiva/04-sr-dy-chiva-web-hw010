import React, { Component } from "react";
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";

export default class CardItem extends Component {
  render() {
    const mystyle = {
      width: 300,
      height: 220
    };
    return (
        <div className="col-sm-4">
          <Card>
            <Card.Img style={mystyle} variant="top"  src={this.props.image}/>
            <Card.Body>
              <Card.Title>{this.props.title}</Card.Title>
              <Card.Text>{this.props.desc}</Card.Text>
              <Link to={`/Posts/${this.props.id}`}>
                {" "}
                see more...
              </Link>
            </Card.Body>
            <Card.Footer>
              <small className="text-muted">Last updated 3 mins ago</small>
            </Card.Footer>
          </Card>

          <br />
        </div>
       
    );
  }
}
