import React from "react";
import CardItem from "./CardItem";

function Home() {
  var card = [
    {
      id: 1,
      title: "Agkor Temple",
      desc: "Angkor Temples Get Your Tickets Now! Best Angkor Wat Tours, Top Activities and Tickets Online.",
      image: "https://drwyjmricaxm7.cloudfront.net/repository/Angkor-Wat-274521447157304_crop_800_600.jpg"
    },
    {
      id: 2,
      title: "Bayon",
      desc: " The Bayon is a richly decorated Khmer temple at Angkor in Cambodia.",
      image: "https://storage.googleapis.com/checkfront-rogue.appspot.com/accounts/cf-65568/images/2018/large-Bayon-Temple-%E2%80%93-Mysterious-Smiling-Faces-1519812721213.jpeg?alt=media&generation=1519812737894246"
    },
    {
      id: 3,
      title: "Taprom temple",
      desc: "Ta Prohm (Khmer: ប្រាសាទតាព្រហ្ម) is the modern name of the temple in Siem Reap, Cambodia",
      image:"https://media.tacdn.com/media/attractions-splice-spp-674x446/09/28/62/aa.jpg"
    },
    {
      id: 4,
      title: "Preah Vihear",
      desc: "Preah Vihear Temple is an ancient Hindu temple built during the period of the Khmer Empire,",
      image: "https://media.tacdn.com/media/attractions-splice-spp-674x446/09/28/62/aa.jpg"
   },
    {
        id: 5,
        title: "Preah Vihear",
        desc: "Preah Vihear Temple is an ancient Hindu temple built during the period of the Khmer Empire,",
        image: "https://media.tacdn.com/media/attractions-splice-spp-674x446/09/28/62/aa.jpg"
      },
      {
        id: 6,
        title: "Preah Vihear",
        desc: "Preah Vihear Temple is an ancient Hindu temple built during the period of the Khmer Empire,",
        image: "https://media.tacdn.com/media/attractions-splice-spp-674x446/09/28/62/aa.jpg"
      
      },
  ];
  return (
    <div>
      <div className="container">
        <div className="row" style={{width: 1000, margin: '0 auto', marginTop:'20px'}}>
          {card.map((item, index) => (
            <CardItem key={index} id={item.id} title={item.title} desc={item.desc} image={item.image} />
          ))}
        </div>
      </div>
    </div>
  );
}

export default Home;
