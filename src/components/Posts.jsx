import React from 'react'

function Posts(props) {
    return (
        <div  className="container" style={{padding:"4%"}}>
            <h1>This is content from post {props.match.params.id}</h1>
        </div>
    )
}

export default Posts
