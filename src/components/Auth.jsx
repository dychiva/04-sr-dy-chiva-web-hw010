import { Form, Button } from "react-bootstrap";
import { Link} from "react-router-dom";

import React from 'react'

function Auth(props) {
  return (
    <div>
       <div>
        <div>
          <Form  style={{width: 400, margin: '0 auto', marginTop:'30px'}}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>User Name</Form.Label>
              <Form.Control type="email" placeholder="username" />
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" placeholder="Password" />
            </Form.Group>

            <Link to="/Welcome">
              <Button variant="primary" type="submit" onClick={props.handleSubmit}>
                Submit
              </Button>
            </Link>
          </Form>
        </div>
      </div>
    </div>
  )
}

export default Auth
