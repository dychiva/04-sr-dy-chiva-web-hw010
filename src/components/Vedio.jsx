import React from "react";
import { Link } from "react-router-dom";

function Vedio(props) {
  const vedioType = props.match.params.id;
  const category = props.match.params.category;

  const Animation = (
    <div>
      <h1>Animation Category</h1>
      <ul>
        <li>
          <Link to="/Vedio/Animation/action">Action</Link>
        </li>
        <li>
          <Link to="/Vedio/Animation/romance">Romance</Link>
        </li>
        <li>
          <Link to="/Vedio/Animation/comedy">Comedy</Link>
        </li>
      </ul>
    </div>
  );

  const Movie = (
    <div>
      <h1>Movie Category</h1>
      <ul>
        <li>
          <Link to="/Vedio/Movie/Adventage">Adventure</Link>
        </li>
        <li>
          <Link to="/Vedio/Movie/Comedy">Comedy</Link>
        </li>
        <li>
          <Link to="/Vedio/Movie/Crime">Crime</Link>
        </li>
        <li>
          <Link to="/Vedio/Movie/Documentory">Documentory</Link>
        </li>
      </ul>
    </div>
  );
  return (
    <div className="container" style={{padding:"4%"}}>
      <ul>
        <li>
          <Link to="/Vedio/Animation">Animation</Link>
        </li>
        <li>
          <Link to="/Vedio/Movie">Movie</Link>
        </li>
      </ul>

      {vedioType === "Animation" ? Animation : null}
      {vedioType === "Movie" ? Movie : null}
      {category === undefined ? (
        <h3>Please select a topic</h3>
      ) : (
        <h3>{category}</h3>
      )}
    </div>
  );
}

export default Vedio;
